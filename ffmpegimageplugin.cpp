/*
 * Qt image plugin using ffmpeg for demuxing/decoding.
 *
 * SPDX-FileCopyrightText: 2021 Martin Sandsmark <martin.sandsmark@kde.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include <QImageIOHandler>
#include <QRect>
#include <QImage>
#include <QFile>
#include <QMimeDatabase>
#include <QUrl>
#include <QtEndian>
#include <iostream>

extern "C" {
#include <unistd.h>
#include <libavutil/imgutils.h>
#include <libavutil/samplefmt.h>
#include <libavutil/timestamp.h>
#include <libavutil/display.h>
#include <libavutil/log.h>
#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>
#include <libswscale/swscale.h>
}

#include "ffmpegimageplugin.h"

#define DECODE_MAX_RETRY 100

Q_LOGGING_CATEGORY(LOGGING, "imageformats.ffmpeg.plugin", QtWarningMsg)
Q_LOGGING_CATEGORY(LOGGING_FFMPEG, "imageformats.ffmpeg.ffmpeg", QtWarningMsg)

// Simple helper to RAII suppress warnings
// Should be placed in the start of all functions calling anything ffmpeg
// Works with recursion as well, thanks to destruction order
struct FFmpegSilencer
{
    int previousLevel = AV_LOG_WARNING;

    FFmpegSilencer() :
        previousLevel(av_log_get_level())
    {
        if (!LOGGING_FFMPEG().isDebugEnabled()) {
            av_log_set_level(AV_LOG_PANIC);
        }
    }

    ~FFmpegSilencer() {
        av_log_set_level(previousLevel);
    }
};

QDebug operator<<(QDebug debug, const AVRational a)
{
    QDebugStateSaver saver(debug);
    debug.nospace() << a.num << '/' << a.den << " (" << av_q2d(a) << ')';

    return debug;
}

class FFmpegImageHandler : public QImageIOHandler
{
    static bool isApng(const QByteArray &buffer, QByteArray *format);
    static bool isWebM(const QByteArray &buffer);
    static bool isAvifSequence(const QByteArray &buffer);
    static bool probeMagicBuffer(const QByteArray &buffer, QByteArray *format);

public:
    explicit FFmpegImageHandler(QIODevice *device);
    ~FFmpegImageHandler() override;

    bool canRead() const override;
    bool read(QImage *image) override;
    QVariant option(ImageOption option) const override;
    void setOption(ImageOption option, const QVariant &value) override;
    bool supportsOption(ImageOption option) const override;
    bool jumpToNextImage() override;
    bool jumpToImage(int imageNumber) override;
    int loopCount() const override;
    int imageCount() const override;
    int nextImageDelay() const override;
    int currentImageNumber() const override;
    QRect currentImageRect() const override;

    bool isInitialized() const { return m_initialized; }

    static bool probeMagic(QIODevice *device, QByteArray *format);
private:
    QSize adjustedFrameSize() const {
        if (!m_scaledSize.isEmpty()) {
            return m_scaledSize;
        }
        if (!m_codecContext) {
            return QSize();
        }
        QSize outSize;
        AVRational aspect{};
        if (m_frame) {
            outSize = QSize(m_frame->width, m_frame->height);
            aspect = av_guess_sample_aspect_ratio(m_formatContext, m_formatContext->streams[m_videoStreamIndex], m_frame);
        }
        if (outSize.isEmpty()) {
            outSize = QSize(m_codecContext->width, m_codecContext->height);
        }
        if (!aspect.num || !aspect.den) {
            aspect = m_codecContext->sample_aspect_ratio;
        }

        // QImage doesn't have non-square pixels (except DPI, but nothing cares about that), so we just scale
        if (aspect.den && aspect.num && av_cmp_q(aspect, {1, 1}) != 0 ) {
            int adjustedWidth = outSize.width();
            int adjustedHeight = outSize.height();
            // We could always scale up, but avoid trying to get redonk big images
            if (outSize.width() > outSize.height()) {
                adjustedWidth *= av_q2d(aspect);
            } else {
                adjustedHeight /= av_q2d(aspect);
            }

            if (adjustedWidth > 0 && adjustedHeight > 0) {
                return QSize(adjustedWidth, adjustedHeight);
            }
        }
        return outSize;
    }
    static bool isAscii(const char *string) {
        if (!string) {
            return false;
        }
        const int len = strlen(string);
        if (!len) {
            return false;
        }
        for (int i=0; i<len; i++) {
            if (string[i] < ' ' || string[i] > '~') {
                return false;
            }
        }
        return true;
    }
    void readMetadata(AVDictionary *tags) {
        if (!tags) {
            return;
        }

        const AVDictionaryEntry *tag = nullptr;
        while ((tag = av_dict_get(tags, "", tag, AV_DICT_IGNORE_SUFFIX))) {
            if (!isAscii(tag->key) || !isAscii(tag->value)) {
                break;
            }
            m_metadata.append({QString::fromLatin1(tag->key), QString::fromLatin1(tag->value)});
        }
    }


    bool fetchFrame(int targetPTS = -1);

    bool initializeFFmpeg();
    bool ensureInitialized(bool needFrame = true) const; // we have to do this stupid dance because of QMovie

    QPointer<QIODevice> m_device;
    bool m_initialized = false;
    bool m_failed = false;

    AVFormatContext *m_formatContext = nullptr;
    AVCodecContext *m_codecContext = nullptr;
    const AVCodec *m_decoder = nullptr;
    AVPacket *m_packet = nullptr;
    AVFrame *m_frame = nullptr;
    int m_videoStreamIndex = -1;

    AVIOContext *m_ioContext = nullptr;

    SwsContext *m_scaleContext = nullptr;
    QSize m_scaledSize;
    AVPixelFormat m_sourcePixFormat = AV_PIX_FMT_NONE;

    int m_manualImageCount = 0;

    int m_quality = 70;
    int m_transformations = -1;
    QVector<QPair<QString, QString>> m_metadata;

    QImage::Format m_imageFormat = QImage::Format_RGB32;
    AVPixelFormat m_swsFormat = AV_PIX_FMT_RGB32;

    bool m_hasFrame = false;
};

bool FFmpegImageHandler::probeMagic(QIODevice *device, QByteArray *format)
{
    const qint64 originalPos = device->pos();

    // kinda arbitrary, 79 is required to recognize webm, and I haven't seen pngs with more than 75 before the idat
    QByteArray buffer = device->peek(100);
    if (probeMagicBuffer(buffer, format)) {
        qCDebug(LOGGING) << "Probe from peeked buffer succeeded with format" << *format;
        return true;
    }
    if (!format->isEmpty()) {
        qCDebug(LOGGING) << "Probe from peeked buffer rejected with format" << *format;
        return false;
    }

    // Wasn't enough when peeking, so let's try with a bigger one
    if (device->isSequential()) {
        return false;
    }
    buffer = device->read(4096);
    device->seek(originalPos);
    if (probeMagicBuffer(buffer, format)) {
        qCDebug(LOGGING) << "Probe from read buffer succeeded with format" << *format;
        return true;
    }
    qCDebug(LOGGING) << "Magic probe failed";
    return false;
}

bool FFmpegImageHandler::probeMagicBuffer(const QByteArray &buffer, QByteArray *format)
{
    if (buffer.startsWith("gimp xcf")) {
        *format = "xcf";
        return false;
    }
    if (buffer.startsWith("GIF89a")) {
        *format = "gif";
        return false;
    }
    if (buffer.startsWith("\x8A\x4D\x4E\x47\x0D\x0A\x1A\x0A")) {
        *format = "mng";
        return false;
    }
    if (buffer.startsWith("\x8B\x4A\x4E\x47\x0D\x0A\x1A\x0A")) {
        *format = "jng";
        return true;
    }
    if (buffer.startsWith("YUV4MPEG2 W")) {
        *format = "y4m";
        return true;
    }
    if (buffer.startsWith("\xFF\xD8\xFF")) {
        *format = "jpg";
        return false;
    }

    if (buffer.startsWith(QByteArray("\0\0\0", 3))) {
        const QByteArray ftyp = buffer.mid(4, 4);
        if (ftyp == "ftyp") {
            const QByteArray variant = buffer.mid(8, 4);
            if (variant == "avif") {
                *format = "avif";
                return false;
            }
            if (variant == "avis") {
                *format = "avifs";
                return true;
            }
            if (variant == "dash") {
                *format = "mp4";
                return true;
            }
            if (variant == "heic" || variant == "heif") {
                *format = "heic";
                return false;
            }
            if (variant == "qt  " || variant == "M4V ") {
                *format = "mov";
                return true;
            }
            if (variant == "jxl ") {
                *format = "jxl";
                return false; // broken in ffmpeg
            }

            const QByteArray type = buffer.mid(8, 3);

            // all the iso (isom, iso5, etc.) and mp4 (mp42 etc.) variants
            if (type == "mp4" || type == "iso") {
                *format = "mp4";
                return true;
            }
            if (type == "3gp") {
                *format = "3gp";
                return true;
            }
            if (type == "FAC") {
                *format = "face";
                return true;
            }
        }
        if (ftyp == "JXL ") {
            *format = "jxl";
            return false; // broken in ffmpeg
        }
    }

    if (buffer.size() >= 79) {
        if (isWebM(buffer)) {
            *format = "webm";
            return true;
        }
    }
    if (isApng(buffer, format)) {
        *format = "apng";
        return true;
    }
    if (*format == "png") {
        return false;
    }

    return false;
}

bool FFmpegImageHandler::isApng(const QByteArray &buffer, QByteArray *format)
{
    if (!buffer.startsWith("\x89\x50\x4E\x47\x0D\x0A\x1A\x0A")) {
        return false;
    }
    // It's at least a png
    *format = "png";

    // Read through as many chunks as available in the buffer, looking for the acTL chunk
    size_t offset = 8;
    while (buffer.size() > offset + 4 + 4 + 4) {// each chunk needs at least chunk length, chunk id and crc
        const char *data = buffer.constData() + offset;
        uint32_t chunkLength;
        memcpy(&chunkLength, data, sizeof chunkLength); // we care about alignment
        chunkLength = qToBigEndian(chunkLength);
        data += 4;
        if (memcmp(data, "acTL", 4) == 0) {
            *format = "apng";
            return true;
        }

        if (memcmp(data, "IDAT", 4) == 0) {
            // acTL must precede IDAT
            return false;
        }

        offset += chunkLength + sizeof chunkLength + 4 + 4; // chunkLength + size of chunklength + chunk id + crc
    }

    return false;
}

bool FFmpegImageHandler::isAvifSequence(const QByteArray &buffer)
{
    if (buffer.size() < 12) {
        return false;
    }
    return memcmp(buffer.constData() + 4, "ftypavis", 8) == 0;
}

bool FFmpegImageHandler::isWebM(const QByteArray &buffer)
{
    if (buffer.size() < 79) {
        return false;
    }

    const char *ebmlID = "\x1a\x45\xdf\xa3";
    if (!buffer.startsWith(ebmlID)) {
        qCDebug(LOGGING) << "Does not start with ebml id";
        return false;
    }

    const char *docID = "\x42\x82";
    int index = buffer.indexOf(docID);
    if (index < 5 || index > 65) {
        qCDebug(LOGGING) << "does not contain doc id";
        return false;
    }

    const char *webmID = "webm";
    index = buffer.indexOf(webmID);
    if (index < 8 || index > 75) {
        qCDebug(LOGGING) << "does not contain webm";
        return false;
    }
    qCDebug(LOGGING) << "Is webm";

    return true;
}

static int avioRead(void *opaque, uint8_t *buf, int buf_size)
{
    QIODevice *device = reinterpret_cast<QIODevice*>(opaque);
    if (!device->isReadable()) {
        qCWarning(LOGGING) << "Device became unavailable before reading!";
        return AVERROR_EXTERNAL;
    }

    qint64 ret = device->read(reinterpret_cast<char*>(buf), buf_size);
    if (ret == 0) {
        if (device->atEnd()) {
            return AVERROR_EOF;
        } else {
            qCWarning(LOGGING) << "Error reading" << device->errorString();
            return AVERROR_EXTERNAL;
        }
    }
    return ret;
}

static int64_t avioSeek(void *opaque, int64_t offset, int whence)
{
    QIODevice *device = reinterpret_cast<QIODevice*>(opaque);
    if (!device->isReadable()) {
        qCWarning(LOGGING) << "Device became unavailable!";
        return AVERROR_EXTERNAL;
    }

    bool success = false;
    switch(whence) {
    case AVSEEK_SIZE:
        return device->size();
    case SEEK_SET:
        success = device->seek(offset);
        break;
    case SEEK_END:
        success = device->seek(device->size() - offset);
        break;
    case SEEK_CUR:
        success = device->seek(device->pos() + offset);
        break;
    default:
        qCWarning(LOGGING) << "Unexpected whence:" << whence;
        break;
    }

    if (!success) {
        qCWarning(LOGGING) << "seek failed";
        return AVERROR_EXTERNAL;
    }

    return device->pos();
}

static QString errorString(int err)
{
    char errbuf[AV_ERROR_MAX_STRING_SIZE ];
    av_strerror(err, errbuf, AV_ERROR_MAX_STRING_SIZE );
    return QString::fromUtf8(errbuf);
}

bool FFmpegImageHandler::initializeFFmpeg()
{
    FFmpegSilencer silencer;

    // If it is a local file, let ffmpeg handle the io, since a lot of Qt stuff
    // likes to mess with the QIODevice prematurely
    QByteArray filename;
    QFile *fileDevice = qobject_cast<QFile*>(m_device);
    if (fileDevice) {
        filename = fileDevice->fileName().toLocal8Bit();

        if (access(filename.constData(), R_OK) != 0) {
            filename.clear();
        }
    }

    m_formatContext = avformat_alloc_context();

    if (filename.isEmpty()) {
        filename = "pipe:qiodevice";

        static constexpr size_t bufferSize = 32768; // size stolen from various places in ffmpeg
        unsigned char *iobuf = reinterpret_cast<unsigned char*>(av_malloc(bufferSize));
        m_ioContext = avio_alloc_context(
                iobuf,
                bufferSize,
                0, // don't enable write
                m_device,
                avioRead,
                nullptr, // write
                avioSeek
                );
        if (!m_device->isSequential()) {
            m_ioContext->seekable  = AVIO_SEEKABLE_NORMAL;
        }
        m_formatContext->pb = m_ioContext;
        m_formatContext->flags |= AVFMT_FLAG_CUSTOM_IO;
    }


    int ret;
    if ((ret = avformat_open_input(&m_formatContext, filename.constData(), nullptr, nullptr)) < 0) {
        qCDebug(LOGGING) << "Unable to open input" << errorString(ret) << filename;
        return false;
    }

    if ((ret = avformat_find_stream_info(m_formatContext, nullptr)) < 0) {
        qCDebug(LOGGING) << "Unable to get stream info" << errorString(ret);
        return false;
    }

    m_videoStreamIndex = av_find_best_stream(m_formatContext, AVMEDIA_TYPE_VIDEO, -1, -1, &m_decoder, 0);
    if (m_videoStreamIndex < 0) {
        qCDebug(LOGGING) << "Unable to find video stream" << errorString(m_videoStreamIndex);
        return false;
    }
    if (!m_decoder) {
        qCDebug(LOGGING) << "Failed to find decoder";
        return false;
    }
    m_codecContext = avcodec_alloc_context3(m_decoder);
    if (!m_codecContext) {
        qCDebug(LOGGING) << "Failed to allocate codec context";
        return false;
    }
    avcodec_parameters_to_context(m_codecContext, m_formatContext->streams[m_videoStreamIndex]->codecpar);
    ret = avcodec_open2(m_codecContext, m_decoder, nullptr);
    if (ret != 0) {
        qCDebug(LOGGING) << "Failed to initialize codec" << errorString(ret);
        return false;
    }

    qCDebug(LOGGING) << "ffmpeg initialized";

    m_packet = av_packet_alloc();

    // Find rotation
    AVStream *stream = m_formatContext->streams[m_videoStreamIndex];
    m_transformations = QImageIOHandler::TransformationNone;
    for (int i=0; i<stream->nb_side_data; i++) {
        if (stream->side_data[i].type != AV_PKT_DATA_DISPLAYMATRIX) {
            continue;
        }
        if (stream->side_data[i].size != sizeof(int32_t) * 9) {
            qCWarning(LOGGING) << "Invalid display matrix size" << stream->side_data[i].size << "expected" << sizeof(int32_t) * 9;
            continue;
        }
        int32_t *matrix = reinterpret_cast<int32_t*>(stream->side_data[i].data);
        double rotation = av_display_rotation_get(matrix);
        if (qFuzzyCompare(rotation, 0.)) {
            m_transformations |= QImageIOHandler::TransformationNone;
        } else if (qFuzzyCompare(rotation, 90.)) {
            m_transformations |= QImageIOHandler::TransformationRotate270;
        } else if (qFuzzyCompare(rotation, 180.) || qFuzzyCompare(rotation, -180.)) {
            m_transformations |= QImageIOHandler::TransformationRotate180;
        } else if (qFuzzyCompare(rotation, -90.)) {
            m_transformations |= QImageIOHandler::TransformationRotate90;
        } else {
            qCWarning(LOGGING) << "Unhandled rotation" << rotation;
            continue;
        }
    }
    readMetadata(m_formatContext->metadata);

    m_initialized = true;
    if (LOGGING().isDebugEnabled()) {
        av_dump_format(m_formatContext, m_videoStreamIndex, filename.constData(), 0);
    }

    return true;
}

bool FFmpegImageHandler::fetchFrame(int targetPTS)
{
    FFmpegSilencer silencer;

    m_hasFrame = false;

    if (!m_frame) {
        m_frame = av_frame_alloc();
    }

    int retries = 0;
    for (; retries < DECODE_MAX_RETRY; retries++) {
        av_packet_unref(m_packet);
        int ret = av_read_frame(m_formatContext, m_packet);
        if (ret == AVERROR(EAGAIN)) {
            continue;
        }
        if (ret != 0) {
            if (ret == AVERROR_EOF) {
                qCDebug(LOGGING) << "EOF";
                m_manualImageCount = currentImageNumber();
                return false; // not fatal
            }
            qCWarning(LOGGING) << "Error while decoding" << errorString(ret);
            m_failed = true;
            return false;
        }
        if (m_packet->stream_index != m_videoStreamIndex) {
            continue;
        }
        ret = avcodec_send_packet(m_codecContext, m_packet);
        if (ret < 0) {
            qCDebug(LOGGING) << "Error sending packet" << errorString(ret);
            m_failed = true;
            return false;
        }
        ret = avcodec_receive_frame(m_codecContext, m_frame);
        if (ret == AVERROR(EAGAIN)) {
            continue;
        }
        if (ret != 0) {
            qCDebug(LOGGING) << "Error receiving packet" << errorString(ret);
            m_failed = true;
            return false;
        }

        // We have to send all packets to the decoder, and we have to consume all frames, so we can't easily skip.
        if (targetPTS > 0 && targetPTS > m_packet->pts) { // webm luckily have pts == dts
            retries--;
            continue;
        }
        m_hasFrame = true;
        return true;
    }

    qCWarning(LOGGING) << "Too many retries" << retries;
    return false;
}

bool FFmpegImageHandler::ensureInitialized(bool needFrame) const
{
    FFmpegSilencer silencer;

    if (m_failed) {
        return false;
    }
    FFmpegImageHandler *handler = const_cast<FFmpegImageHandler*>(this);
    if (m_initialized) {
        if (needFrame && !m_frame) {
            qCDebug(LOGGING) << "Already initialized, but missing frame";
            return handler->fetchFrame();
        }
        return true;
    }

    if (!handler->initializeFFmpeg()) {
        handler->m_failed = true;
        return false;
    }

    if (needFrame && !m_frame) {
        qCDebug(LOGGING) << "Initialized and requested frame";
        return handler->fetchFrame();
    }

    return true;
}

FFmpegImageHandler::FFmpegImageHandler(QIODevice *device) :
    m_device(device)
{
}

FFmpegImageHandler::~FFmpegImageHandler()
{
    FFmpegSilencer silencer;

    if (m_codecContext) {
        avcodec_free_context(&m_codecContext);
    }

    if (m_formatContext) {
        avformat_close_input(&m_formatContext);
        avformat_free_context(m_formatContext);
    }

    if (m_frame) {
        av_frame_free(&m_frame);
    }
    if (m_scaleContext) {
        sws_freeContext(m_scaleContext);
    }
    if (m_packet) {
        av_packet_unref(m_packet);
        av_packet_free(&m_packet);
    }
    if (m_ioContext) {
        av_freep(&m_ioContext->buffer);
        av_freep(&m_ioContext);
    }
}

bool FFmpegImageHandler::read(QImage *image)
{
    FFmpegSilencer silencer;

    if (!ensureInitialized()) {
        return false;
    }

    if (!m_hasFrame) {
        return false;
    }

    const QSize outSize = adjustedFrameSize();
    if (outSize.isEmpty()) {
        qCWarning(LOGGING) << "Can't render to empty frame";
        return false;
    }

    AVPixelFormat framePixFormat = AVPixelFormat(m_frame->format);

    bool needToSetRange = false;
    switch (framePixFormat) {
    case AV_PIX_FMT_YUVJ420P:
        framePixFormat = AV_PIX_FMT_YUV420P;
        needToSetRange = true;
        break;
    case AV_PIX_FMT_YUVJ422P:
        framePixFormat = AV_PIX_FMT_YUV422P;
        needToSetRange = true;
        break;
    case AV_PIX_FMT_YUVJ444P:
        framePixFormat = AV_PIX_FMT_YUV444P;
        needToSetRange = true;
        break;
    case AV_PIX_FMT_YUVJ440P:
        framePixFormat = AV_PIX_FMT_YUV440P;
        needToSetRange = true;
        break;
    default:
        break;
    }

    if (!m_scaleContext || m_sourcePixFormat != framePixFormat) {
        m_sourcePixFormat = framePixFormat;
        sws_freeContext(m_scaleContext);
        int scaleFlags = -1;
        if (m_quality == 100) {
            scaleFlags = SWS_LANCZOS;
        } else if (m_quality == 99) { // idk I'm not a scaling specialist, you should really just use QImage's scaling instead
            scaleFlags = SWS_SPLINE;
        } else if (m_quality > 70) {
            scaleFlags = SWS_BICUBIC;
        } else if (m_quality > 50 || m_quality == -1) {
            scaleFlags = SWS_BILINEAR;
        } else {
            scaleFlags = SWS_FAST_BILINEAR;
        }
        m_scaleContext = sws_getContext(m_frame->width, m_frame->height, framePixFormat, outSize.width(), outSize.height(), m_swsFormat, scaleFlags, nullptr, nullptr, nullptr);
        if (needToSetRange) {
            int dummy[4];
            int srcRange, dstRange;
            int brightness, contrast, saturation;
            sws_getColorspaceDetails(m_scaleContext, (int**)&dummy, &srcRange, (int**)&dummy, &dstRange, &brightness, &contrast, &saturation);
            const int* coefs = sws_getCoefficients(SWS_CS_DEFAULT);
            srcRange = 1; // this marks that values are according to yuvj
            sws_setColorspaceDetails(m_scaleContext, coefs, srcRange, coefs, dstRange,
                                     brightness, contrast, saturation);


        }
    }

    // It's a bit ugly, but it makes it easier to pass in a custom deleter to QImage and avoid a copy
    uint8_t **destData = new uint8_t*[AV_NUM_DATA_POINTERS]();
    int linesizes[AV_NUM_DATA_POINTERS];
    av_image_alloc(destData, linesizes, outSize.width(), outSize.height(), m_swsFormat, 32);

    sws_scale(m_scaleContext, m_frame->data, m_frame->linesize, 0, m_frame->height, destData, linesizes);

    *image = QImage(*destData, outSize.width(), outSize.height(), linesizes[0], m_imageFormat,
        [](void *d) {
            av_freep(d);
            delete[] reinterpret_cast<uint8_t**>(d);
        },
        destData
    );
    for (const QPair<QString, QString> &item : m_metadata) {
        image->setText(item.first, item.second);
    }

    fetchFrame();

    return true;
}

QVariant FFmpegImageHandler::option(ImageOption option) const
{
    FFmpegSilencer silencer;

    switch(option) {
    case Animation: {
        static const QSet<QString> knownStatic {
            "fits",
            "exr",
            "dpx",
            "pcx",
            "dds",
            "psd",
            "svg",
            "sgi",
            "xbm",
            "xwd",
        };
        if (knownStatic.contains(format())) {
            qCDebug(LOGGING) << " - Queried for animation, but format is static" << format();
            return false;
        }
        if (!ensureInitialized()) {
            qCWarning(LOGGING) << "ffmpeg failed to load!";
            return false;
        }
        const int frames = imageCount();
        qCDebug(LOGGING) << " - Queried for animation for format" << format() << "imagecount:" << frames;
        // ffmpeg thinks apng has 0 frames, but reads them all
        return frames == 0 || frames > 1;
    }
    case ScaledSize:
        qCDebug(LOGGING) << " - Queried for scaled size";
        return m_scaledSize;
    case Size:
        qCDebug(LOGGING) << " - queried size";
        if (!m_initialized) {
            qCDebug(LOGGING) << "Asked for size before initialized!";
            if (!ensureInitialized(false)) {
                qCDebug(LOGGING) << "failed to load to fetch size";
                return QVariant();
            }
        }
        qCDebug(LOGGING) << "size" << adjustedFrameSize();
        return adjustedFrameSize();
    case Quality:
        return m_quality;
    case ImageFormat:
       return QImage::Format_RGB32;
    case Description: {
        if (!ensureInitialized()) {
            qCWarning(LOGGING) << "failed to load to read description";
            return false;
        }
        QStringList list;
        for (const QPair<QString, QString> &item : m_metadata) {
            list.append(item.first + ": " + item.second);
        }
        return list.join("\n\n");
    }
    case SubType: {
       if (!ensureInitialized(false)) {
           qCWarning(LOGGING) << "failed to load to read subtype";
           return QVariant();
       }
       QByteArray subtype = avcodec_descriptor_get(m_formatContext->streams[m_videoStreamIndex]->codecpar->codec_id)->name;
       qCDebug(LOGGING) << " subtype:" << subtype;
       if (subtype == format()) {
           return "";
       }
       return subtype;
    }
    case ImageTransformation: {
        if (m_transformations != -1) {
            return m_transformations;
        }
       if (!ensureInitialized(false)) {
           qCWarning(LOGGING) << "failed to load to read transformation";
           return QVariant();
       }
       return int(m_transformations);
    }
    default:
        qCDebug(LOGGING) << " - Queried for unsupported" << option;
        break;
    }
    return QVariant();
}

void FFmpegImageHandler::setOption(ImageOption option, const QVariant &value)
{
    FFmpegSilencer silencer;

    ensureInitialized(false);
    switch(option) {
    case ScaledSize:
        m_scaledSize = value.toSize();
        if (m_scaleContext) {
            sws_freeContext(m_scaleContext);
            m_scaleContext = nullptr;
        }
        break;
    case Quality:
        m_quality = value.toInt();
        if (m_scaleContext) {
            sws_freeContext(m_scaleContext);
            m_scaleContext = nullptr;
        }
        break;
        // TODO
    case ImageFormat: {
        QImage::Format format = QImage::Format(value.toInt());
        switch(format) {
        case QImage::Format_RGB32:
            m_swsFormat = AV_PIX_FMT_RGB32;
            break;
        case QImage::Format_Grayscale8:
            m_swsFormat = AV_PIX_FMT_GRAY8;
            break;
        case QImage::Format_Grayscale16:
            m_swsFormat = AV_PIX_FMT_YA16;
            break;
        case QImage::Format_Mono:
            m_swsFormat = AV_PIX_FMT_MONOBLACK;
            break;
        case QImage::Format_RGB888:
            m_swsFormat = AV_PIX_FMT_RGB24;
            break;
        case QImage::Format_RGB444:
            m_swsFormat = AV_PIX_FMT_RGB444;
            break;
        case QImage::Format_BGR888:
            m_swsFormat = AV_PIX_FMT_BGR24;
            break;
        case QImage::Format_RGB16:
            m_swsFormat = AV_PIX_FMT_RGB565;
            break;
        case QImage::Format_RGB30:
            m_swsFormat = AV_PIX_FMT_X2RGB10;
            break;
        case QImage::Format_RGBA64:
            m_swsFormat = AV_PIX_FMT_RGBA64;
            break;

            // can't be bothered to extract the palette
        //case QImage::Format_Indexed8:
        //    m_swsFormat = AV_PIX_FMT_PAL8;
        //    break;
        default:
            return;
        }
        m_imageFormat = format;
        if (m_scaleContext) {
            sws_freeContext(m_scaleContext);
            m_scaleContext = nullptr;
        }
    }
    default:
        qCDebug(LOGGING) << "set unsupported option" << option;
        break;
    }
}

bool FFmpegImageHandler::supportsOption(ImageOption option) const
{
    if (!m_initialized) {
        qCDebug(LOGGING) << "asked for option before init";
    }
    switch(option) {
    case Animation:
    case ScaledSize:
    case Size:
    case Quality:
    case ImageFormat:
    case SubType:
        //qCDebug(LOGGING) << " - Queried for support for imageformat";
        return true;
    case ImageTransformation:
        return true;
    case ClipRect:
    case ScaledClipRect:
        //qCDebug(LOGGING) << " - Queried for supports option" << option;
        // todo
        return false;
    case BackgroundColor:
    case IncrementalReading:
        return false;
    case Description:
        return true;
    case CompressionRatio:
    case Gamma:
    case Name:
    case Endianness:
    case SupportedSubTypes:
    case OptimizedWrite:
    case ProgressiveScanWrite:
        qCDebug(LOGGING) << " - Queried support for unsupported" << option;
        break;
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    case TransformedByDefault:
        return false;
#endif
    default:
        qCDebug(LOGGING) << " - Queried support for unknown" << option;
        break;
    }
    return false;
}

bool FFmpegImageHandler::jumpToNextImage()
{
    FFmpegSilencer silencer;

    if (!ensureInitialized()) {
        return false;
    }
    if (imageCount() > 0 && currentImageNumber() >= imageCount()) {
        avformat_seek_file(m_formatContext, -1, 0, 0, 0, AVSEEK_FLAG_FRAME);
        avcodec_flush_buffers(m_codecContext);
    }
    qCDebug(LOGGING) << "Jumping to next";
    return fetchFrame();
}

bool FFmpegImageHandler::jumpToImage(int imageNumber)
{
    FFmpegSilencer silencer;

    if (!ensureInitialized()) {
        return false;
    }
    if (imageNumber == currentImageNumber()) {
        return true;
    }
    if (imageCount() > 0 && imageNumber >= imageCount()) {
        qCWarning(LOGGING) << "Can't seek that far" << imageNumber << imageCount();
    }
    qCDebug(LOGGING) << "Jumping to" << imageNumber << "/" << imageCount();
    const int positionBefore = currentImageNumber();
    int ret = avformat_seek_file(m_formatContext, -1, imageNumber, imageNumber, imageNumber, AVSEEK_FLAG_FRAME | AVSEEK_FLAG_ANY);
    if (ret < 0) {
        qCWarning(LOGGING) <<" Failed to seek from" << positionBefore << "to" << imageNumber << "of total" << imageCount() << errorString(ret);
        avformat_seek_file(m_formatContext, -1, 0, 0, 0, AVSEEK_FLAG_FRAME);
        avcodec_flush_buffers(m_codecContext);
        return false;
    }

    avcodec_flush_buffers(m_codecContext);

    const AVRational fps = av_guess_frame_rate(m_formatContext, m_formatContext->streams[m_videoStreamIndex], m_frame);
    const AVRational timeBase = m_formatContext->streams[m_videoStreamIndex]->time_base;
    const int targetPTS = imageNumber * fps.den * timeBase.den / (timeBase.num * fps.num);
    return fetchFrame(targetPTS);
}

int FFmpegImageHandler::loopCount() const
{
    // Loop forever
    // TODO: we can fetch it from some decoders
    return -1;
}

int FFmpegImageHandler::imageCount() const
{
    FFmpegSilencer silencer;

    if (!m_initialized) {
        qCDebug(LOGGING) << "Queried count before initialize!";
    }
    if (!ensureInitialized(false)) {
        return -1;
    }
    int frames = m_formatContext->streams[m_videoStreamIndex]->nb_frames;
    if (frames) {
        return frames;
    }

    int64_t duration = m_formatContext->streams[m_videoStreamIndex]->duration;
    if (duration == AV_NOPTS_VALUE && m_formatContext->duration == AV_NOPTS_VALUE) {
        return m_manualImageCount;
    }
    duration = m_formatContext->duration;

    const AVRational fps = m_formatContext->streams[m_videoStreamIndex]->avg_frame_rate;
    if (fps.num == 0) {
        return -1;
    }
    const int64_t frameTime = fps.den * AV_TIME_BASE / fps.num;
    return m_formatContext->duration / frameTime - 1;
}

int FFmpegImageHandler::nextImageDelay() const
{
    FFmpegSilencer silencer;

    if (!m_initialized) {
        qCDebug(LOGGING) << "Queried delay before initialize!";
    }
    if (!ensureInitialized()) {
        return -1;
    }
    if (m_frame && m_frame->pkt_duration > 0) {
        const AVRational timeBase = m_formatContext->streams[m_videoStreamIndex]->time_base;
        int ret = 1000 * timeBase.num * m_frame->pkt_duration / timeBase.den;
        if (ret > 1) {
            return ret;
        }

        // There are some broken codecs which use the wrong time base
        const AVRational codecTimeBase = m_codecContext->time_base;
        ret = 1000 * codecTimeBase.num * m_frame->pkt_duration / codecTimeBase.den;
        if (ret > 1) {
            return ret;
        }
    }

    const AVRational fps = av_guess_frame_rate(m_formatContext, m_formatContext->streams[m_videoStreamIndex], m_frame);
    if (!fps.num || !fps.den) {
        return 16;
    }
    return 1000 * fps.den / fps.num;
}

int FFmpegImageHandler::currentImageNumber() const
{
    FFmpegSilencer silencer;

    if (m_failed) {
        return -1;
    }
    if (!ensureInitialized(false)) {
        return -1;
    }
    if (!m_frame) {
        return 1;
    }
    if (!m_frame->pts) {
        return 0;
    }

    uint64_t pts = m_frame->pts;//m_frame->best_effort_timestamp;
    if (pts == AV_NOPTS_VALUE) {
        pts = m_frame->pts;
    }
    if (pts == AV_NOPTS_VALUE) {
        pts = m_frame->pkt_dts;
    }
    if (pts == AV_NOPTS_VALUE) {
        qCDebug(LOGGING) << "missingall timestamps";
        return 0;
    }
    AVStream *stream = m_formatContext->streams[m_videoStreamIndex];
    const AVRational fps = stream->avg_frame_rate;
    if (!fps.den) {
        qCDebug(LOGGING) << "missing FPS";
        return 0;
    }

    AVRational num = av_mul_q(stream->time_base, av_mul_q(fps, av_make_q(pts, 1)));
    return std::ceil(av_q2d(num));
}

QRect FFmpegImageHandler::currentImageRect() const
{
    if (!m_scaledSize.isEmpty()) {
        return QRect(QPoint(0, 0), m_scaledSize);
    }
    if (!m_initialized) {
        qCDebug(LOGGING) << "Queried image rect before initialize!";
    }
    if (!ensureInitialized()) {
        return QRect();
    }

    return QRect(QPoint(0, 0), adjustedFrameSize());
}

#define PROBE_BUF_MAX (1 << 20)
static bool ffmpegProbeFormat(QIODevice *device, const QByteArray &pluginFormat = "", QByteArray *formatName = nullptr)
{
    FFmpegSilencer silencer;

    if (!device) {
        return false;
    }
    QByteArray peeked = device->peek(4096);
    QByteArray mimeName;
    QMimeDatabase mimeDb;
    QMimeType mimeType = mimeDb.mimeTypeForName("image/" + pluginFormat);
    if (!pluginFormat.isEmpty()) {
        if (!mimeType.isValid() || mimeType.isDefault()) {
            mimeType = mimeDb.mimeTypeForName("video/" + pluginFormat);
        }
    }
    if (!peeked.isEmpty()) {
        if (!mimeType.isValid() || mimeType.isDefault()) {
            mimeType = mimeDb.mimeTypeForData(peeked);
        }
    }
    mimeName = mimeType.name().toLatin1();
    qCDebug(LOGGING) << "Probing with mime name" << mimeName;

    AVProbeData probe;
    probe.filename = nullptr;
    probe.mime_type = nullptr;
    if (!mimeName.isEmpty()) {
        probe.mime_type = mimeName.constData();
    } else {
        probe.mime_type = nullptr;
    }
    int score_max = AVPROBE_SCORE_MAX / 4 + 1;

    // av_probe_input_format3 is a bit off, so it might try to read a long straddling the end of the buffer,
    // so keep the last 8 bytes secret from it.
    if (peeked.size() > 8) {
        probe.buf = (unsigned char*)peeked.data();
        probe.buf_size = peeked.size() - 8;
        const AVInputFormat *format = av_probe_input_format3(&probe, 1, &score_max);
        if (format) {
            if (formatName) {
                *formatName = format->name;
            }
            qCDebug(LOGGING) << "found with peeked" << format->name << "score:" << score_max;
            qCDebug(LOGGING) << "mimetype" << format->mime_type << "long name" << format->long_name << "name" << format->name << "extensions" << format->extensions;
            // idk, should we free the format? I can't see anything doing that in ffmpeg proper
            return true;
        }
    }
    if (device->isSequential()) {
        return false;
    }
    qCDebug(LOGGING) << "Probing with buffer only failed, trying full read";
    qint64 originalPos = device->pos();
    device->seek(0);
    static constexpr size_t bufferSize = 32768; // size stolen from various places in ffmpeg
    unsigned char *iobuf = reinterpret_cast<unsigned char*>(av_malloc(bufferSize));
    AVIOContext *ioContext = avio_alloc_context(
            iobuf,
            bufferSize,
            0, // don't enable write
            device,
            avioRead,
            nullptr, // write
            avioSeek
        );
    const AVInputFormat *format = nullptr;
    int confidence = av_probe_input_buffer2(
            ioContext,
            &format,
            nullptr, // filename
            nullptr, // log class
            0, // offset
            0 // max probe size
        );
    device->seek(originalPos);
    av_freep(&ioContext->buffer);
    av_freep(&ioContext);

    if (!format) {
        return false;
    }

    qCDebug(LOGGING) << "Probe successful with io context:" << format->name << confidence;
    if (formatName) {
        *formatName = format->name;
    }

    return true;
}

bool FFmpegImageHandler::canRead() const
{
    FFmpegSilencer silencer;

    if (m_failed) {
        return false;
    }
    if (!m_device || !m_device->isReadable()) {
        qCWarning(LOGGING) << "Device unreadable!";
        return false;
    }
    if (!ensureInitialized(false)) {
        return false;
    }
    if (!m_frame) {
        // Assume true until we know it isn't
        return true;
    }

    const int frames = imageCount();
    // Otherwise mister qmovie gets confused
    if (frames > 0 && currentImageNumber() >= frames) {
        return false;
    }
    return m_hasFrame;
}

static bool isBroken(const QByteArray &format)
{
    static const QSet<QByteArray> brokenFormats{
        // ffmpeg thinks it can decode it with its png decoder, but it's wrong
        "mng",

        // These are misidentified
        "heic",

        // ffmpeg can only do sequences, not plain avif
        "avif",

        //"heif",
        // We also don't want to be responsible for these, qt has builtin support:
        "png",
        "png_pipe", // ffmpeg's name
        "jpg",
        "jpeg",
        "jpeg_pipe", // ffmpeg's name
        "xpm",
        "gif",

        "mka", // no video

        "rawvideo", // requires knowing the size
        "xcf", // spends too long
        "webp", // fails with animated webp's
        "jxl", // misidentified as mp4 by ffmpeg, and broken
    };
    return brokenFormats.contains(format);
}

static bool isWorking(const QByteArray &format)
{
    static const QSet<QByteArray> knownWorking{
        "webm",
        "avifs",
        "flic",
        "apng",
        "mkv",
        "mov",
        "mp4",
        "avi",
        "mjpeg",
        "asf",
        "flv",
        "fits",
        "exr",
        "dpx",
        "pcx", // about twice as fast as kimageformats implementation
        "dds", // also separate qt plugin
        "psd", // also kimageformats
        "sgi",
        "svg", // separate qt support
        "xbm", // qt builtin
        "xwd", // x window dump
        "y4m", // uncompressed YCbCr frame(s)
        "mpegts",
        "wmv",
        "dv",
        "bink",
        "mpeg",
    };
    return knownWorking.contains(format);
}

static QByteArray matchWorkingFormat(const QList<QByteArray> &formats)
{
    for (const QByteArray &format : formats) {
        if (isWorking(format)) {
            return format;
        }
    }
    return "";
}

static const bool s_handleUnknownFormats = qEnvironmentVariableIsSet("QT_FFMPEG_IMAGEFORMATS_ALL");
static const bool s_disable = qEnvironmentVariableIsSet("QT_FFMPEG_IMAGEPLUGIN_DISABLE");

QImageIOPlugin::Capabilities FFmpegImagePlugin::capabilities(QIODevice *device, const QByteArray &format) const
{
    if (s_disable) {
        qCDebug(LOGGING) << "Disabled";
        return Capabilities();
    }

    FFmpegSilencer silencer;

    qCDebug(LOGGING) << "Queried for capabilities for format" << format;

    if (isWorking(format)) {
        qCDebug(LOGGING) << "Format known working";
        return CanRead;
    }

    if (!device || !device->isReadable()) {
        qCDebug(LOGGING) << "No device or not readable";
        return Capabilities();
    }

    QByteArray probedFormat;
    if (FFmpegImageHandler::probeMagic(device, &probedFormat)) {
        qCDebug(LOGGING) << "Format" << probedFormat << "accepted by probe";
        return CanRead;
    } else if (!probedFormat.isEmpty()) {
        qCDebug(LOGGING) << "Probed format" << probedFormat << "rejected by probe";
        return Capabilities();
    }

    if (isBroken(probedFormat)) {
        qCDebug(LOGGING) << "probed format" << probedFormat << "is blacklisted";
        return Capabilities();
    }

    if (!ffmpegProbeFormat(device, format, &probedFormat)) {
        qCDebug(LOGGING) << "probe failed";
        return Capabilities();
    }

    if (s_handleUnknownFormats) {
        qCDebug(LOGGING) << "Probe succeeded, format" << format << "and all formats enabled";
        return CanRead;
    }

    const QByteArray workingFormat = matchWorkingFormat(probedFormat.split(','));
    if (!workingFormat.isEmpty()) {
        qCDebug(LOGGING) << "Probe succeeded, format" << workingFormat;
        return CanRead;
    }

    qCDebug(LOGGING) << "QT_FFMPEG_IMAGEFORMATS_ALL is not set, so not trying to load unknown format" << probedFormat;
    return Capabilities();
}

QImageIOHandler *FFmpegImagePlugin::create(QIODevice *device, const QByteArray &format) const
{
    FFmpegSilencer silencer;

    qCDebug(LOGGING) << "asked to create for" << format;

    if (!device || !device->isReadable()) {
        qCDebug(LOGGING) << "missing device";
        return nullptr;
    }
    if (!(capabilities(device, format) & CanRead)) {
        qCWarning(LOGGING) << "Asked to create for rejected format" << format; // should never happen
    }

    // Just so we can set the correct format to QImageReader
    QByteArray probedFormat;
    FFmpegImageHandler::probeMagic(device, &probedFormat);
    if (probedFormat.isEmpty()) {
        ffmpegProbeFormat(device, format, &probedFormat);
    }

    if (probedFormat.endsWith("_pipe")) {
        probedFormat.chop(5);
    }
    qCDebug(LOGGING) << "Loading format" << probedFormat;
    FFmpegImageHandler *handler = new FFmpegImageHandler(device);
    if (!probedFormat.isEmpty()) {
        handler->setFormat(probedFormat);
    }

    // We can't initialize here, because QMovie is a shitlord and fucks with our device
//    if (!handler->isInitialized()) {
//        qCWarning(LOGGING) << "INitialize failed";
//        delete handler;
//        return nullptr;
//    }
    return handler;
}
