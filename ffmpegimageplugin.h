/*
 * Qt image plugin using ffmpeg for demuxing/decoding.
 *
 * SPDX-FileCopyrightText: 2021 Martin Sandsmark <martin.sandsmark@kde.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#include <QImageIOPlugin>
#include <QLoggingCategory>

class FFmpegImagePlugin: public QImageIOPlugin {
  Q_OBJECT
  Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QImageIOHandlerFactoryInterface" FILE "extensions.json")
public:
  Capabilities capabilities(QIODevice *device, const QByteArray &format) const override;
  QImageIOHandler *create(QIODevice *device, const QByteArray &format) const override;
};

Q_DECLARE_LOGGING_CATEGORY(LOGGING);
Q_DECLARE_LOGGING_CATEGORY(LOGGING_FFMPEG); // for ffmpeg itself
